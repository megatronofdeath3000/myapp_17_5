﻿// MyApp_17_5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class Vector
{
public:
    Vector()
    {}
    Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}
    void Length()
    {
        std::cout << "Vector length : " << sqrt(x * x + y * y + z * z);
    }
private:
    double x = 2;
    double y = 0;
    double z = 0;

};

int main()
{
    Vector v;
    v.Length();
}

